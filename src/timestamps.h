#include <unistd.h>
#include <stdio.h>
#include <sys/time.h>
#include <stddef.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>

typedef struct node {
    void *frame;
    struct node * next;
} timestamp_list_t;

#ifdef __cplusplus
extern "C" {
#endif

#define EVENT_INIT_START            "init_start"
#define EVENT_ITER_START            "iter_start"
#define EVENT_WORK_START            "work_start"
#define EVENT_REDIST_START          "redist_start"
#define EVENT_ADAPT_START           "adapt_start"
#define EVENT_FINALIZE_START        "finalize_start"

#define EVENT_INIT_END              "init_end"
#define EVENT_ITER_END              "iter_end"
#define EVENT_WORK_END              "work_end"
#define EVENT_REDIST_END            "redist_end"
#define EVENT_ADAPT_END             "adapt_end"
#define EVENT_FINALIZE_END          "finalize_end"



void disable_timestamps(void);
void enable_timestamps(void);
int timestamps_enabled(void);

/*
 ************* INIT, TIMESTAMPS, PRINT ****************      
 */
timestamp_list_t * timestamp_list_new();

void make_timestamp_init_start(timestamp_list_t * list);
void make_timestamp_init_end(timestamp_list_t * list, int num_procs);
void make_timestamp_iter_start(timestamp_list_t * list, int num_procs, int iter);
void make_timestamp_iter_end(timestamp_list_t * list, int num_procs, int iter);
void make_timestamp_adapt_start(timestamp_list_t * list, int num_procs, int iter);
void make_timestamp_adapt_end(timestamp_list_t * list, int num_procs, int iter);
void make_timestamp_iter_end(timestamp_list_t * list, int num_procs, int iter);
void make_timestamp_finalize_start(timestamp_list_t * list, int num_procs, int iter);
void make_timestamp_finalize_end(timestamp_list_t * list, int num_procs, int iter);
void make_timestamp(timestamp_list_t *list, char *event_name, int num_procs, int iteration);

long get_diff(timestamp_list_t *list, int index1, int index2, char *ev1, char *ev2);

void print_list_to_file(timestamp_list_t * head,  const char *filename);

void timestamp_list_free(timestamp_list_t ** list);


#ifdef __cplusplus
}
#endif
