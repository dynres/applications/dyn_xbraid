#include "timestamps.h"

static int enabled_flag = 1;


/* Micro Benchmark frame */
typedef struct{
    char event_name[64];
    long timestamp;
    int iteration;
    int num_procs;
}timing_frame_micro_t;

/*
 * LIST FUNCTIONS
 */

void push(timestamp_list_t * head, void *val) {
    timestamp_list_t * current = head;
    while (current->next != 0) {
        current = current->next;
    }

    /* now we can add a new variable */
    current->next = (timestamp_list_t *) malloc(sizeof(timestamp_list_t));
    current->next->frame = val;
    current->next->next = 0;
}

int pop(timestamp_list_t ** head) {
    int retval = -1;
    timestamp_list_t * next_node = 0;

    if (*head == 0) {
        return -1;
    }

    next_node = (*head)->next;
    free((*head)->frame);
    free(*head);
    *head = next_node;
    return retval;
}

void enable_timestamps(){
    enabled_flag = 1;
}

void disable_timestamps(){
    enabled_flag = 0;
}

int timestamps_enabled(){
    return enabled_flag;
}

/* Init */
timestamp_list_t * timestamp_list_new(){
    if(0 == enabled_flag){
        return NULL;
    }
    return (timestamp_list_t *)calloc(1, sizeof(timestamp_list_t)); 
}

/* Timestamps */
void make_timestamp_init_start(timestamp_list_t * list){
    make_timestamp(list, "init_start", 1, 0);
}

/* Timestamps */
void make_timestamp_init_end(timestamp_list_t * list, int num_procs){
    make_timestamp(list, "init_end", num_procs, 0);
}

void make_timestamp_iter_start(timestamp_list_t * list, int num_procs, int iter){
    make_timestamp(list, "iter_start", num_procs, iter);
}

void make_timestamp_iter_end(timestamp_list_t * list, int num_procs, int iter){
    make_timestamp(list, "iter_end", num_procs, iter);
}

void make_timestamp_adapt_start(timestamp_list_t * list, int num_procs, int iter){
    make_timestamp(list, "adapt_start", num_procs, iter);
}

void make_timestamp_adapt_end(timestamp_list_t * list, int num_procs, int iter){
    make_timestamp(list, "adapt_end", num_procs, iter);
}

void make_timestamp_finalize_start(timestamp_list_t * list, int num_procs, int iter){
    make_timestamp(list, "finalize_start", num_procs, iter);
}

void make_timestamp_finalize_end(timestamp_list_t * list, int num_procs, int iter){
    make_timestamp(list, "finalize_end", num_procs, iter);
}

/* Timestamps */
void make_timestamp(timestamp_list_t *list, char *event_name, int num_procs, int iteration){
    if(0 == enabled_flag){
        return;
    }
    timing_frame_micro_t * m_frame = calloc(1, sizeof(timing_frame_micro_t));
    push(list, (void*) m_frame);

    struct timeval timestamp;
    gettimeofday(&timestamp, 0);
    
    m_frame->timestamp = timestamp.tv_sec * 1000000 + timestamp.tv_usec;
    strcpy(m_frame->event_name, event_name);
    m_frame->num_procs = num_procs;
    m_frame->iteration = iteration;
}

long get_diff(timestamp_list_t *list, int index1, int index2, char *ev1, char *ev2){
    timestamp_list_t * current = list->next;
    long ts1, ts2;
    int ev1_it = 0;
    int ev2_it = 0;
    int f1 = 0, f2 = 0;

    if(0 == enabled_flag){
        return -1;
    }


    while (current != 0) {
        timing_frame_micro_t *timing = (timing_frame_micro_t *) current->frame;
        if(0 == strcmp(timing->event_name, ev1)){
            if(ev1_it == index1){
                ts1 = timing->timestamp;
                f1 = 1;
            } 
            ev1_it++;
        }

        if(0 == strcmp(timing->event_name, ev2)){
            if(ev2_it == index2){
                ts2 = timing->timestamp;
                f2 = 1;
            } 
            ev2_it++;
        }
        if(f1 && f2){
            return ts2 - ts1;
        }
        current = current->next;
    }
    return -1;
}

/* Write */
void print_list_to_file(timestamp_list_t * head,  const char *filename) {

    if(0 == enabled_flag){
        return;
    }
 
    FILE *f = fopen(filename, "a+");
    if(NULL == f){
        printf("Error opening file: %s\n", strerror(errno));
        return;
    }

    char header[] = "#timestamp,event_name,num_procs,iteration";
    fprintf(f, "%s\n", header);

    timestamp_list_t * current = head->next;
    head->next = 0;
    while (current != 0) {
        timing_frame_micro_t *timing = (timing_frame_micro_t *) current->frame;

        fprintf(f, "%ld,%s,%d,%d\n",   timing->timestamp, timing->event_name, timing->num_procs, timing->iteration);
        pop(&current);
    }
    fclose(f);
}

void timestamp_list_free(timestamp_list_t ** list){

    if(0 == enabled_flag){
        return;
    }

    timestamp_list_t * head = *list;
    timestamp_list_t * current = head->next;
    while (current != 0) {
        pop(&current);
    }

    free(head);    
}